/******************************************************************************/
/*!
\file   ObjectAllocator.cpp
\author Jonathan Owens
\par    email: j.owens\@digipen.edu
\par    DigiPen login: j.owens
\par    Course: CS280
\par    Assignment #1
\date   Due: 01.20.2016

\brief
Contains implemntations for our custom memory manager

Functions include:
  + ObjectAllocator
  + ~ObjectAllocator
  + Allocate
  + Free
  + DumpMemoryInUse
  + ValidatePages
  + FreeEmptyPages
  + ImplementedExtraCredit
  + SetDebugState
  + GetFreeList
  + GetPageList
  + GetConfig
  + GetStats
  + allocate_new_page
  + populate_free_list
  + update_header
  + is_memory_in_use
  + is_padding_corrup

Hours Spend: 20+

Difficulties:
  + Pointer arithmatic
  + Casting between types
  + Couldn't figure out how to walk through the page list to grab all the
    leftover external headers and free them

*/
/******************************************************************************/

#include "ObjectAllocator.h"
#include <memory.h>
#include <cstring>
#include <string>

  // so I don't have to type out the cast every time
#define CastGenericObject reinterpret_cast<GenericObject*>
#define HeaderType _config.HBlockInfo_.type_

#define LABEL_SIZE 16

/****************************************************************************/
/*!

\brief
Constructor, allocates a page and populates the free list

\param ObjectSize
Size of each object

\param config
Various configuration information

*/
/****************************************************************************/
ObjectAllocator::ObjectAllocator(size_t ObjectSize, const OAConfig& config)
{
    // setting private data
  _pageList = NULL;
  _freeList = NULL;
  _counter  = 0;
  _config = config;



    // determine size of page, accounting for headers, padding, etc
  size_t pageSize = (ObjectSize * config.ObjectsPerPage_)               +
                   ((config.PadBytes_ * config.ObjectsPerPage_) * 2)    +
                    (config.HBlockInfo_.size_ * config.ObjectsPerPage_) +
                     sizeof(void *);

    // setting known statistics
  _stats.ObjectSize_ = ObjectSize;
  _stats.PageSize_   = pageSize;

   // allocating new page
  allocate_new_page(pageSize);
}

/****************************************************************************/
/*!

\brief
Destructor, deletes any headers that were not deleted and deletes pages

*/
/****************************************************************************/
ObjectAllocator::~ObjectAllocator()
{
  while(_pageList)
  {
    GenericObject* free = _pageList;
    _pageList = _pageList->Next;
    delete [] free;
  }
}

/****************************************************************************/
/*!

\brief
Gives the client memory from the free list

\param label
Label for external header

\return
A pointer to the block of memory

*/
/****************************************************************************/
void* ObjectAllocator::Allocate(const char *label)
{
  if(_stats.FreeObjects_ == 0)
  {
    if(_config.MaxPages_ > _stats.PagesInUse_ || !_config.DebugOn_)
      allocate_new_page(_stats.PageSize_);
    else
    {

      throw OAException(OAException::E_NO_PAGES, "No logical memory");
    }
  }

  // take an object from free list and give it to client
  GenericObject *clientNew = _freeList;
  if(clientNew)
    _freeList = _freeList->Next;

  clientNew->Next = NULL;
    // updating stats during allocation
  _stats.ObjectsInUse_++;
  _stats.FreeObjects_--;
  _stats.Allocations_++;
  _stats.MostObjects_++;

    // setting the allocated bytes
  memset(clientNew, ALLOCATED_PATTERN, _stats.ObjectSize_);
    // updating header info
  update_header(clientNew, true, label);

  return clientNew;
}

/****************************************************************************/
/*!

\brief
Returns the memory from the client to the free list

\param Object
The memory they want to free

*/
/****************************************************************************/
void ObjectAllocator::Free(void *Object)
{
  if(_config.DebugOn_)
  {
    // checking for bad boundary
   GenericObject* page = _pageList;
   size_t blockSize = _stats.ObjectSize_ + (_config.PadBytes_ * 2) + _config.HBlockInfo_.size_;
   size_t offset = _config.HBlockInfo_.size_ + _config.PadBytes_ + sizeof(void*);

   while(page)
   {
     if(page <= Object && Object <= (reinterpret_cast<char*>(page) + _stats.PageSize_))
     {
       if((reinterpret_cast<char*>(Object) - ((reinterpret_cast<char*>(page) + offset))) % static_cast<int>(blockSize) != 0)
         throw OAException(OAException::E_BAD_BOUNDARY, "Trying to free a bad boundary");
     }
     page = page->Next;
   }
      // checking for double free
   GenericObject* freeList = _freeList;

   while(freeList)
   {
     if(freeList == Object)
       throw OAException(OAException::E_MULTIPLE_FREE, "Already freed this memory");

     freeList = freeList->Next;
   }

      // checking for corrupt blocks
  if(is_padding_corrupt(Object))
    throw OAException(OAException::E_CORRUPTED_BLOCK, "Padding is corrupted");
      // setting memory to allocated pattern
    memset(Object, FREED_PATTERN, _stats.ObjectSize_);
    GenericObject* tempFree = CastGenericObject(Object);

    tempFree->Next = _freeList;
    _freeList = tempFree;
  }

      // updating deallocation stats
    _stats.FreeObjects_++;
    _stats.ObjectsInUse_--;
    _stats.Deallocations_++;

    update_header(Object, false, NULL);
}

/****************************************************************************/
/*!

\brief
Dumps all blocks of memory that are being used by the client

\param fn
Pointer to a function that you pass the in-use blocks to

\return
The number of blocks in use

*/
/****************************************************************************/
unsigned ObjectAllocator::DumpMemoryInUse(DUMPCALLBACK fn) const
{
  size_t offset = sizeof(void*) + _config.PadBytes_ + _config.HBlockInfo_.size_;

  GenericObject* pageWalk = _pageList;

while(pageWalk)
{
  unsigned char* pWalk = reinterpret_cast<unsigned char*>(pageWalk) + offset;
  unsigned char* end = reinterpret_cast<unsigned char*>(pWalk) + (_stats.PageSize_ - offset);

  while(pWalk < end)
  {
    if(is_memory_in_use(pWalk))
      fn(pWalk, _stats.ObjectSize_);

    pWalk += (_config.PadBytes_ * 2) + _config.HBlockInfo_.size_ + _stats.ObjectSize_;
  }
  pageWalk = pageWalk->Next;
}
return _stats.ObjectsInUse_;

}

/****************************************************************************/
/*!

\brief
Validates all pad bytes to make sure they haven't been corrupted

\param fn
Pointer to a function that takes the corrupted objects

\return
Number of corrupt objects

*/
/****************************************************************************/
unsigned ObjectAllocator::ValidatePages(VALIDATECALLBACK fn) const
{
  if(_config.PadBytes_ == 0 || !_config.DebugOn_)
    return 0;

  GenericObject* pageWalk = _pageList;
  size_t offset = sizeof(void*) + _config.PadBytes_ + _config.HBlockInfo_.size_;

  unsigned corruptions = 0;
  while(pageWalk)
  {
    unsigned char* pWalk = reinterpret_cast<unsigned char*>(pageWalk) + offset;
    unsigned char* end = reinterpret_cast<unsigned char*>(pWalk) + (_stats.PageSize_ - offset);

    while(pWalk < end)
    {
      if(is_padding_corrupt(pWalk))
      {
        fn(pWalk, _stats.ObjectSize_);
        corruptions++;
      }
      pWalk += (_config.PadBytes_ * 2) + _config.HBlockInfo_.size_ + _stats.ObjectSize_;
    }
    pageWalk = pageWalk->Next;
  }


  return corruptions;
}

/****************************************************************************/
/*!

\brief
Not implemented

\return
N/A

*/
/****************************************************************************/
unsigned ObjectAllocator::FreeEmptyPages(void)
{
  return 0;
}

/****************************************************************************/
/*!

\brief
Checks if EC was implemented

\return
Was it implemented?

*/
/****************************************************************************/
bool ObjectAllocator::ImplementedExtraCredit(void)
{
  return false;
}

/****************************************************************************/
/*!

\brief
Sets the debug state of the object allocator

\param State
Boolean for debug state true/false

*/
/****************************************************************************/
void ObjectAllocator::SetDebugState(bool State)
{
  _config.DebugOn_ = State;
}

/****************************************************************************/
/*!

\brief
Returns pointer to free list to client

\return
A pointer to the free list

*/
/****************************************************************************/
const void* ObjectAllocator::GetFreeList(void) const
{
  return _freeList;
}

/****************************************************************************/
/*!

\brief
Returns a pointer to the page list to the client

\return
A pointer to the page list

*/
/****************************************************************************/
const void* ObjectAllocator::GetPageList(void) const
{
  return _pageList;
}

/****************************************************************************/
/*!

\brief
Passes config struct to caller

\return
Configuration struct

*/
/****************************************************************************/
OAConfig ObjectAllocator::GetConfig(void) const
{
  return _config;
}

/****************************************************************************/
/*!

\brief
Returns stats info to caller

\return
The stats struct

*/
/****************************************************************************/
OAStats ObjectAllocator::GetStats(void) const
{
  return _stats;
}

/****************************************************************************/
/*!

\brief
Allocates the page for the constructor

\param bytes
Size of the page to allocate

*/
/****************************************************************************/
void ObjectAllocator::allocate_new_page(size_t bytes)
{
    // pointer to page
  char *page = NULL;

  try
  {
    page = new char[bytes]; // allocated the new page
  }

  catch (std::bad_alloc &)
  {
    throw OAException(OAException::E_NO_MEMORY,
                      "allocate_new_page: No system memory available"
                     );
  }

    // setting memory hex values
  memset(page, UNALLOCATED_PATTERN, bytes);

    // if there is already a page allocated
  if(_pageList)
  {
      // create a new one and set the original's next* to the original
      // page and set page list to the new page
    GenericObject *newPage = CastGenericObject(page);
    GenericObject *temp = _pageList;
    _pageList = newPage;
    _pageList->Next = temp;
  }
  else
  {
    _pageList = CastGenericObject(page);
    _pageList->Next = NULL;
  }
    // updating stats for new page allocation
  _stats.PagesInUse_++;
  _stats.FreeObjects_ += _config.ObjectsPerPage_;

    // populating free list with newly allocated page
  populate_free_list(_pageList);

}

/****************************************************************************/
/*!

\brief
Populates the free list with all free objects

\param Object
The pagelist

*/
/****************************************************************************/
void ObjectAllocator::populate_free_list(void *Object)
{
    // getting a pointer to the end of the page
  char* end = static_cast<char*>(Object) + _stats.PageSize_;
    // pointer beginning just after the next pointer
  char *pWalk = static_cast<char*>(Object) +
                sizeof(void*)              +
                GetConfig().PadBytes_      +
                GetConfig().HBlockInfo_.size_;

    // while the first pointer is not passed the end pointer
  while(pWalk < end)
  {
      // settin pad bytes (beginning and end of objects)
    memset(pWalk - _config.PadBytes_, PAD_PATTERN, _config.PadBytes_);
    memset(pWalk + _stats.ObjectSize_, PAD_PATTERN, _config.PadBytes_);
    memset(pWalk - _config.PadBytes_ - _config.HBlockInfo_.size_, 0, _config.HBlockInfo_.size_);


      // create a object pointer and setting it to the current pointer position
    GenericObject *freeNode = CastGenericObject(pWalk);
    freeNode->Next = _freeList;
    _freeList = freeNode;
    pWalk += _stats.ObjectSize_ + (GetConfig().PadBytes_ * 2) + GetConfig().HBlockInfo_.size_;
  }
}

/****************************************************************************/
/*!

\brief
Updates header information for various types of headers

\param block
The object to update the header for

\param allocating
Are we updating because of an allocation or a free

\param label
Label for exter headers

*/
/****************************************************************************/
void ObjectAllocator::update_header(void *block, bool allocating, const char* label)
{
  size_t headerLoc =_config.PadBytes_ + _config.HBlockInfo_.size_;
  unsigned char *pBlock = static_cast<unsigned char*>(block);
  int allocations = static_cast<int>(_stats.Allocations_);

  if(_config.DebugOn_)
  {

    if(HeaderType == GetConfig().hbNone)
      return;

      // basic header
    if(HeaderType == GetConfig().hbBasic)
    {
      memset(pBlock - GetConfig().PadBytes_ - (sizeof(char)),
             (allocating) ? 1 : 0,
             sizeof(char));

      memset(pBlock - GetConfig().PadBytes_ -
             sizeof(char) * 5, // 5 bytes from the right of the header
             (allocating) ? allocations : 0,
             sizeof(char));
    }

      // extended header
    if(HeaderType == GetConfig().hbExtended)
    {
      int *counter = reinterpret_cast<int*>(pBlock - headerLoc + _config.HBlockInfo_.additional_);

        // allocated or not
      memset(pBlock - _config.PadBytes_ - (sizeof(char)),
             allocating ? 1 : 0,
             sizeof(char));

        // how many times has block been used
      if(allocating)
        memset(counter, (*counter)+= 1, sizeof(char));

        // total allocations
      memset(pBlock - _config.PadBytes_ -
             sizeof(char) * 5,
             allocating ? allocations : 0,
             sizeof(char));
    }
  }

    // if using external headers
  if(HeaderType == GetConfig().hbExternal)
  {
      // do this when client is allocating
    if(allocating)
    {
      memset(pBlock - headerLoc, 0, sizeof(void*));
      MemBlockInfo** header = reinterpret_cast<MemBlockInfo**>(pBlock - headerLoc);
      *header = new MemBlockInfo;

      if(label)
      {
        (*header)->label = new char[strlen(label)];
        strncpy((*header)->label, label, strlen(label));
      }
      else
        (*header)->label = NULL;

      (*header)->in_use = true;
      (*header)->alloc_num = _stats.Allocations_;
    }

      // do this when client is deallocating
    else
    {
      MemBlockInfo** header = reinterpret_cast<MemBlockInfo**>(pBlock - headerLoc);
      if((*header)->label)
        delete [] (*header)->label;
      delete(*header);
      memset(header, 0, sizeof(void*));
    }
  }
}

/****************************************************************************/
/*!

\brief
Checks if a memory block is being used by the client

\param object
The object you want to check

\return
Is the memory being used boolean

*/
/****************************************************************************/
bool ObjectAllocator::is_memory_in_use(void *object) const
{
    //no header
  if(HeaderType == _config.hbNone)
  {
    GenericObject* pWalk = _freeList;

    while(pWalk)
    {
      if(pWalk == object)
        return false;

      pWalk = pWalk->Next;
    }
    return true;
  }

    //basic/extended header
  else if(HeaderType == _config.hbExtended || HeaderType == _config.hbBasic)
  {
    unsigned char* useFlag = static_cast<unsigned char*>(object) - _config.PadBytes_ - sizeof(char);

    if(*useFlag == 0)
      return false;
    else if(*useFlag == 1)
      return true;
  }

    //external header
  else if(HeaderType == _config.hbExternal)
  {
    unsigned char* temp = static_cast<unsigned char*>(object) - _config.PadBytes_ - _config.HBlockInfo_.size_;
    MemBlockInfo* header = reinterpret_cast<MemBlockInfo*>(temp);

    if(header && header->in_use == true)
      return true;
    else
      return false;
  }

  return false;
}

/****************************************************************************/
/*!

\brief
Checks if padding is corrupted surrounding an object

\param Object
The object you want to check for corruption

\return
Is the object corrupt boolean

*/
/****************************************************************************/
bool ObjectAllocator::is_padding_corrupt(void *Object) const
{
    // checking for corrupt blocks
  unsigned char* padFront = static_cast<unsigned char*>(Object);
  padFront = padFront - _config.PadBytes_;

  unsigned char* padBack = static_cast<unsigned char*>(Object);
  padBack = padBack + _stats.ObjectSize_;

  for(unsigned int i = 0; i < _config.PadBytes_; i++)
  {
   if((*padFront) != PAD_PATTERN || (*padBack) != PAD_PATTERN)
     return true;

   padFront++;
   padBack++;
  }

  return false;
}
